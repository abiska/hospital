package hospital;

import java.util.*;

public class EmergencyDepartment {

    public static void main(String[] args) {
       
        Wristband wrb1 = new Wristband("123","Covid-19");
        Wristband wrb2 = new Wristband("456","Covid-20");
        Wristband wrb3 = new Wristband("789","Covid-21");
        
        Patient pt1 = new Patient("Sherlock Holmes", "01/01/2000", "James Moriarty", wrb1);
        Patient pt2 = new Patient("Spider Man", "02/02/2000", "Venom", wrb2);
        Patient pt3 = new Patient("Batman", "03/03/2000", "Superman", wrb3);
        
        
        List<Patient> patients = new ArrayList<Patient>();
        patients.add(pt1);
        patients.add(pt2);
        patients.add(pt3);
        
        ResearchGroup rgrp = new ResearchGroup(patients);
        
        List<Patient> pts = rgrp.getTotalPatients();
        
        for(Patient lol : patients){
            System.out.println("\nName: " + lol.getName() + "\nDate of birth: " + lol.getDateOfBirth() + 
                                "\nFamily Doctor: " + lol.getFamilyDoctor());
        }
    
        
    }
    
}
