package hospital;


public class Wristband {
    
    //attribute(s)
    private String barcode;
    private String patientMedicalInfo;
    
    //constructor(s)
    public Wristband(String barcode, String medicalInfo) {
        this.barcode = barcode;
        this.patientMedicalInfo = medicalInfo;
    }
    
    //getter(s)
    public String getBarcode() {
        return barcode;
    }
    public String getPatientMedicalInfo() {
        return patientMedicalInfo;
    }
 
    
}
