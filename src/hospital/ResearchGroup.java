package hospital;


import java.util.*;

public class ResearchGroup {
    
    //attribute(s)
    private List<Patient> patients;
    
    public ResearchGroup (List<Patient>patients){
        this.patients = patients; 
    }
    
    //getter(s)
    public List<Patient> getTotalPatients(){
        return patients;
    }
    
    
}
