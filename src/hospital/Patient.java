package hospital;

import java.util.*;

public class Patient {
    
    //attribute(s)
    private String name;
    private String dateOfBirth;
    private String familyDoctor;
    private Wristband wristband;
    
    //constructor(s)
    public Patient(String name, String dateOfBirth, String familyDoctor, Wristband wristband) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.familyDoctor = familyDoctor;
        this. wristband =  wristband;
    }
    
    //getter(s)
    public String getName() {
        return name;
    }
    public String getDateOfBirth() {
        return dateOfBirth;
    }
    public String getFamilyDoctor() {
        return familyDoctor;
    }
    
    
}
